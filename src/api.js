import axios from "axios";

import { BASE_URL } from "./services/constants/constants";


const getToken = () => {
  const token = localStorage.getItem("token"); // Retrieve token from session storage
  return token;
};

export const apiCall = async (endpoint, method, data = null,params=null) => {
  try {
    const token = getToken();

    const response = await axios({
      method: method,
      url: `${BASE_URL}${endpoint}`,
      data: data,
      params:params,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    return response.data;

  } catch (error) {

    console.error("API Error:", error);

    throw error;

  }
};
