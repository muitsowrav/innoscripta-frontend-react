import {
  FETCH_SOURCES_SUCCESS,
  FETCH_SOURCES_FAILURE,
} from "../constants/constants";

const initialState = {
  sources: [],
  error: null,
};

const sourcesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SOURCES_SUCCESS:
      return {
        ...state,
        sources: action.sources,
        error: null,
      };
    case FETCH_SOURCES_FAILURE:
      return {
        ...state,
        sources: [],
        error: action.error,
      };
    default:
      return state;
  }
};

export default sourcesReducer;
