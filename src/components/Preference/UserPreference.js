import React, { useState } from "react";
import ErrorMessage from "../Layout/ErrorMessage";
import Select from "react-select";
import CustomButton from "../Layout/CustomButton";
import axios from "axios";
import { apiCall } from "../../api";

const UserPreference = ({ sources, authors, categories, loading, error }) => {
  const [selectedSources, setSelectedSources] = useState([]);
  const [selectedAuthors, setSelectedAuthors] = useState([]);
  const [selectedCategories, setSelectedCategories] = useState([]);

  const handleSourceChange = (selectedOptions) => {
    setSelectedSources(selectedOptions);
  };

  const handleAuthorChange = (selectedOptions) => {
    setSelectedAuthors(selectedOptions);
  };

  const handleCategoryChange = (selectedOptions) => {
    setSelectedCategories(selectedOptions);
  };

  const savePreferences = async (preferenceData) => {
    try {
      await apiCall("api/user-preferences", "POST", preferenceData);
      console.log("Preferences saved successfully");
    } catch (error) {
      console.error("Error saving preferences:", error);
    }
  };


  const handleSubmit = () => {
   

    const preferenceData = {
      sources: selectedSources.map((source) => source.id),
      authors: selectedAuthors.map((author) => author.id),
      categories: selectedCategories.map((category) => category.id),
    };

    savePreferences(preferenceData);
  };

  return (
    <div className="max-w-md mx-auto px-4 sm:px-6 lg:px-8">
      <div className="bg-blue-200 shadow-md rounded-lg p-6 mt-8">
        <h1 className="text-2xl font-bold mb-4 text-center">User Preference</h1>
        {loading && <div>Loading...</div>}
        {error && <ErrorMessage message={error} />}
        <div className="mb-4">
          <h3 className="text-lg font-bold">Sources</h3>
          <Select
            options={sources}
            getOptionLabel={(source) => source.name}
            getOptionValue={(source) => source.id}
            isMulti
            className="mt-1"
            value={selectedSources}
            onChange={handleSourceChange}
          />
        </div>
        <div className="mb-4">
          <h3 className="text-lg font-bold">Authors</h3>
          <Select
            options={authors}
            getOptionLabel={(author) => author.name}
            getOptionValue={(author) => author.id}
            isMulti
            value={selectedAuthors}
            onChange={handleAuthorChange}
            className="mt-1"
          />
        </div>
        <div className="mb-4">
          <h3 className="text-lg font-bold">Categories</h3>
          <Select
            options={categories}
            getOptionLabel={(category) => category.name}
            getOptionValue={(category) => category.id}
            isMulti
            className="mt-1"
            value={selectedCategories}
            onChange={handleCategoryChange}
            classNamePrefix="react-select"
          />
        </div>

        <CustomButton
          onClick={handleSubmit}
          bgColor="bg-blue-500"
          hoverColor="hover:bg-blue-700"
          text="Submit"
          textColor="text-white"
        />
      </div>
    </div>
  );
};

export default UserPreference;
