import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import UserPreference from "./UserPreference";
import { fetchAuthors, fetchCategories, fetchSources } from "../../services/actions/preferenceAction";

const UserPreferenceContainer = () => {
  const [isLoading, setIsLoading] = useState(false);
  const[error,setError] = useState(null);
  const dispatch = useDispatch();
  const {

    sourcesReducer: { sources },
    authorsReducer: { authors },
    categoriesReducer: { categories },
  } = useSelector((state) => state);
  useEffect(() => {
    try {
      setIsLoading(true);
      
       dispatch(fetchSources());
       dispatch(fetchAuthors());
      dispatch(fetchCategories());
     
     
      setIsLoading(false);
      
      
    } catch (error) {
      
      setError(error);
    }
   
  }, []);

  return (

    <UserPreference
      sources={sources}
      authors={authors}
      categories={categories}
      loading={isLoading}
      error={error}
    />
  );
};

export default UserPreferenceContainer;
