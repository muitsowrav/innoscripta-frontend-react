import React, { useState } from "react";

const Article = ({
  source,
  category,
  author,
  loading = false,
  title,
  description,
  content,
  date,
}) => {
  const [expanded, setExpanded] = useState(false);

  const handleToggle = () => {
    setExpanded(!expanded);
  };

  return (
    <div>
      <article className="m-2 p-4 bg-white rounded-lg sm:w-full">
        <h2 className="text-2xl font-bold mb-2" onClick={handleToggle}>
          {title}
        </h2>

        {expanded && (
          <>
            <p>
              <strong>Source:</strong> {source}
            </p>
            <p>
              <strong>Category:</strong> {category}
            </p>
            <p>
              <strong>Author:</strong> {author}
            </p>
            <p className="text-gray-800 mt-2">{description}</p>
            <p className="text-gray-800 mt-2">{content}</p>
            <p>
              <strong>Date:</strong> {date}
            </p>
          </>
        )}
      </article>
    </div>
  );
};

export default Article;
