import React, { useState } from "react";
import Select from "react-select";
import fetchArticles from "../../services/actions/articleActions";
import SearchInput from "../Layout/SearchInput";
import { useDispatch } from "react-redux";
import MyDatePicker from "../Layout/MyDatepicker";

const Filters = ({
  sources,
  categories,
  setSearchTerm,
  searchTerm,
  setSelectedSourcesIds,
  setSelectedCategoriesIds,
  setSelectedDate,
  handleSubmit,
  selectedDate
}) => {
  // const [searchTerm, setSearchTerm] = useState("");
  const [selectedSources, setSelectedSources] = useState([]);
  // const [selectedSourcesIds, setSelectedSourcesIds] = useState([]);
  const [selectedCategories, setSelectedCategories] = useState([]);
  // const [selectedCategoriesIds, setSelectedCategoriesIds] = useState([]);
  // const [selectedDate, updateDate] = useState(null);

  const dispatch = useDispatch();
  let ids = selectedCategories.map((category) => category.id);

  const handleSearchTermChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const handleSourceChange = (selectedOptions) => {
    setSelectedSourcesIds(selectedOptions.map((option) => option.id));
    setSelectedSources(selectedOptions);
  };

  const handleCategoryChange = (selectedOptions) => {
    setSelectedCategoriesIds(selectedOptions.map((option) => option.id));
    setSelectedCategories(selectedOptions);
  };

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  // const handleFilterSubmit = () => {
  //   dispatch(
  //     fetchArticles(
  //       searchTerm,
  //       selectedSourcesIds,
  //       selectedCategoriesIds,
  //       selectedDate
  //     )
  //   );
  // };

  return (
    <>
      <div className="my-4 flex flex-col md:flex-row md:items-center">
        <div className="flex-grow md:w-1/2 md:mr-2">
          <SearchInput value={searchTerm} onChange={handleSearchTermChange} />
        </div>
        <div className="flex-grow md:w-1/2">
          <MyDatePicker
            selectedDate={selectedDate}
            handleDateChange={handleDateChange}
          />
        </div>
      </div>
      <div className="flex space-x-2">
        <div className="w-1/2">
          <Select
            options={sources}
            getOptionLabel={(source) => source.name}
            getOptionValue={(source) => source.id}
            isMulti
            placeholder="Select source"
            className="w-full"
            onChange={handleSourceChange}
            value={selectedSources}
          />
        </div>
        <div className="w-1/2">
          <Select
            options={categories}
            getOptionLabel={(category) => category.name}
            getOptionValue={(category) => category.id}
            isMulti
            placeholder="Select category"
            className="w-full"
            onChange={handleCategoryChange}
            value={selectedCategories}
          />
        </div>
      </div>
      <div className="my-4">
        <button
          onClick={handleSubmit}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        >
          Filter
        </button>
      </div>
    </>
  );
};

export default Filters;
