import React from "react";

const PaginationComponent = ({ meta, onPageChange }) => {
  const pagesPerSection = 4;

  const currentSection = () => {
    return Math.ceil(meta.current_page / pagesPerSection);
  };

  const numberOfSections = () => {
    return Math.ceil(meta.last_page / pagesPerSection);
  };

  const getLastPage = () => {
    if (currentSection() === numberOfSections()) {
      return meta.last_page;
    }
    return (currentSection() - 1) * numberOfSections() + pagesPerSection;
  };

  const pages = () => {
    const currentPage = (currentSection() - 1) * pagesPerSection + 1;
    return calcPageRange(currentPage, getLastPage() + 1);
  };

  const changePage = (page) => {
    if (page <= 0 || page > meta.last_page) {
      return;
    }
    onPageChange(page);
  };

  const calcPageRange = (start, end) => {
    return Array.from({ length: end - start }, (_, index) => index + start);
  };

  const navigateSection = (direction) => {
    const value = direction === "next" ? +1 : -1;
    const section = currentSection() + value;
    const firstPageOfSection = (section - 1) * pagesPerSection + 1;
    changePage(firstPageOfSection);
  };

  return (
    <div>
      {!meta ? (
        <div>Loading..</div>
      ) : (
        <div className="flex items-center">
          <div className="flex items-center">
            <button
              onClick={() => changePage(meta.current_page - 1)}
              className={`mr-2 py-2 inline-flex items-center text-sm leading-5 font-medium rounded-lg text-gray-500 focus:outline-none transition ease-in-out duration-150 ${
                meta.current_page === 1
                  ? "cursor-not-allowed"
                  : "hover:text-gray-800 hover:bg-gray-100"
              }`}
            >
              Prev
            </button>
          </div>
          <ul className="flex pl-0 list-none rounded">
            {currentSection() > 1 && (
              <>
                <button
                  onClick={() => changePage(1)}
                  className="px-2 py-1 text-center inline-flex items-center text-sm leading-5 font-medium hover:text-gray-800 hover:bg-gray-100 rounded-lg focus:outline-none"
                >
                  1
                </button>
                <button
                  onClick={() => navigateSection("prev")}
                  className="px-2 py-1 text-center inline-flex items-center text-sm leading-5 font-medium rounded-lg focus:outline-none cursor-pointer"
                >
                  ...
                </button>
              </>
            )}
            {pages().map((page) => (
              <li key={page}>
                <button
                  onClick={() => changePage(page)}
                  href="#"
                  className={`px-2 py-1 text-center inline-flex items-center text-sm leading-5 font-medium hover:text-gray-800 hover:bg-gray-100 rounded-lg focus:outline-none ${
                    meta.current_page === page ? "bg-indigo-500 text-white" : ""
                  }`}
                >
                  {page}
                </button>
              </li>
            ))}
            {currentSection() < numberOfSections() && (
              <>
                <button
                  onClick={() => navigateSection("next")}
                  className="px-2 py-1 text-center inline-flex items-center text-sm leading-5 font-medium rounded-lg focus:outline-none cursor-pointer"
                >
                  ...
                </button>
                <button
                  onClick={() => changePage(meta.last_page)}
                  href="#"
                  className="px-2 py-1 text-center inline-flex items-center text-sm leading-5 font-medium hover:text-gray-800 hover:bg-gray-100 rounded-lg focus:outline-none"
                >
                  {meta.last_page}
                </button>
              </>
            )}
          </ul>
          <div className="flex justify-end">
            <button
              onClick={() => changePage(meta.current_page + 1)}
              className={`ml-2 py-1 inline-flex items-center text-sm leading-5 font-medium rounded-lg text-gray-500 focus:outline-none ${
                meta.current_page === meta.last_page
                  ? "cursor-not-allowed"
                  : "hover:text-gray-800 hover:bg-gray-100"
              }`}
            >
              Next
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default PaginationComponent;
