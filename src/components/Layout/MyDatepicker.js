import React from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const MyDatePicker = ({ selectedDate, handleDateChange }) => {
  
  return (
    <DatePicker
      selected={selectedDate}
      onChange={handleDateChange}
      className="w-full"
      placeholderText="Select date"
    />
  );
};

export default MyDatePicker;
