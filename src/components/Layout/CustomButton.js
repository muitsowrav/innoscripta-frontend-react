import React from 'react'

const CustomButton = ({
  bgColor,
  hoverColor,
  textColor,
  text,
  onClick,
}) => {
  
  return (
    
    <button
      className={`${bgColor} ${hoverColor} ${textColor} font-bold py-2 px-4 m-4 rounded focus:outline-none focus:shadow-outline`}
      onClick={onClick}
    >
      {text}
    </button>
  );
};
export default CustomButton
